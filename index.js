var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var x = 20;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  socket.on('update', function(msg){
    x = x + parseFloat(msg);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

sendUpdate = function() { io.emit('update', x.toString()) }
setInterval( sendUpdate, 100);